import React from 'react';
import { StatusBar } from 'react-native';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk'

import { AppNavigator } from './navigation/AppNavigation';
import bookingsReducer from './store/reducers/bookings';
import sellersReducer from './store/reducers/sellers';
import authReducer from './store/reducers/auth';
import Colors from './utils/Colors';

const rootReducer = combineReducers({
  booking: bookingsReducer,
  seller: sellersReducer,
  auth: authReducer
});

const store = createStore(rootReducer, applyMiddleware(thunk));

const App = () => {
  return (
      <Provider store={store}>
        <StatusBar
          backgroundColor={Colors.header}
          barStyle="light-content"
        />
        <AppNavigator/>
      </Provider>
  );
};

export default App;
