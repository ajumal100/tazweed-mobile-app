export default class Seller{
    constructor(id, name, desc, img, rating, services, location, type) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.img = img;
        this.rating = rating;
        this.services = services;
        this.location = location;
        this.type = type;
    }
}