export default class Booking{
    constructor(id, user, seller, date, hour, status) {
        this.id = id;
        this.user = user;
        this.seller = seller;
        this.date = date;
        this.hour = hour;
        this.status = status;
    }
}