import React from 'react';
import { Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, CardStyleInterpolators, HeaderStyleInterpolators } from '@react-navigation/stack';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import { useSelector } from 'react-redux';


import NearScreen from '../screens/NearScreen';
import ProfileScreen from '../screens/ProfileScreen';
import StartScreen from '../screens/StartScreen';
import AuthScreen from '../screens/AuthScreen';
import SearchScreen from '../screens/SearchScreen';
import BookingsScreen from '../screens/BookingsScreen';
import SellerDetailScreen from '../screens/SellerDetailScreen';
import SearchField from '../components/SearchField';
import Colors from '../utils/Colors';

const defaultHeaderStyles = {
    headerStyle: {
        backgroundColor: Colors.header
    },
    headerTintColor: "#ffffff",
    headerTitleStyle: {
        fontFamily: "Opensans-Regular"
    }
}

export const AppNavigator = () => {
    const isAuth = useSelector(state => state.auth.isAuthenticated);
    return  <NavigationContainer>
                {
                    isAuth ? <AppTabsNavigator/> : <StartupScreenNavigator/>
                }
            </NavigationContainer>
}

const StartupScreen = createStackNavigator();

export const StartupScreenNavigator = () => {
    return <StartupScreen.Navigator>
        <StartupScreen.Screen 
            name="Start"
            component={StartScreen}
            options={{
                headerTitle: "",
                headerTransparent: true
            }}
        />
        <StartupScreen.Screen 
            name="Auth"
            component={AuthScreen}
            options = {(props) => {
                return {
                    headerTitle: props.route.params.type == "login" ? "Sign in":"Create account",
                    headerStyle: {
                        backgroundColor: "#FB7F5B"
                    },
                    headerTintColor: "#ffffff",
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                    headerStyleInterpolator: HeaderStyleInterpolators.forUIKit
                }
            }}
        />
    </StartupScreen.Navigator>
}

const NearMeStack = createStackNavigator();

export const NearMeStackNavigator = () => {
    return <NearMeStack.Navigator>
        <NearMeStack.Screen 
            name="NearMe" 
            component={NearScreen}
            options={{
                ...defaultHeaderStyles,
                headerTitle: "Near by Services",
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
            }}
        />
        <NearMeStack.Screen 
            name="Seller"
            component={SellerDetailScreen}
            options={(props) => {
                return {
                    ...defaultHeaderStyles,
                    headerTitle: props.route.params.screenName,
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                }
            }}
        />
    </NearMeStack.Navigator>
}

const ExploreStack = createStackNavigator();

export const ExploreStackNavigator = () => {
    return <ExploreStack.Navigator>
        <ExploreStack.Screen 
            name="Explore"
            component={SearchScreen}
            options={{
                ...defaultHeaderStyles,
                header: () => {
                    return <SearchField/>
                }
            }}
        />
        <ExploreStack.Screen 
            name="Seller"
            component={SellerDetailScreen}
            options={(props) => {
                return {
                    ...defaultHeaderStyles,
                    headerTitle: props.route.params.screenName
                }
            }}
        />
    </ExploreStack.Navigator>
}

const BookingsStack = createStackNavigator();

export const BookingsStackNavigator = () => {
    return <BookingsStack.Navigator>
        <BookingsStack.Screen 
            name="Appointments"
            component={BookingsScreen}
            options={{
                ...defaultHeaderStyles,
                headerTitle: "My Appointments"
            }}
        />
    </BookingsStack.Navigator>
}

const ProfileStack = createStackNavigator();

export const ProfileStackNavigator = () => {
    return <ProfileStack.Navigator>
        <ProfileStack.Screen 
            name="Profile"
            component={ProfileScreen}
            options={{
                ...defaultHeaderStyles,
                headerTitle: "My Profile"
            }}
        />
    </ProfileStack.Navigator>
}

const AppTabs = createMaterialBottomTabNavigator();

export const AppTabsNavigator = () => {
    return <AppTabs.Navigator
            activeColor="#f0edf6"
            inactiveColor="#3e2465"
            barStyle={{ backgroundColor: "#292F3B", fontSize: 30 }}
            backBehavior="initialRoute"
            inactiveColor="rgba(255,255,255,.4)"
            shifting={false}>
        <AppTabs.Screen 
            name="NearMe" 
            component={NearMeStackNavigator}
            titleStyle={{ fontSize: 30 }}
            options={{
                tabBarLabel: 'Near Me',
                tabBarIcon: ({ color, size = 24 }) => (
                    <Icon name="md-home" color={color} size={size} />
                ),
                tabBarColor: "#006D6A"
            }}
            
        />
        <AppTabs.Screen 
            name="Explore" 
            component={ExploreStackNavigator}
            options={{
                tabBarLabel: 'Explore',
                tabBarIcon: ({ color, size = 24 }) => (
                    <Icon name="md-search" color={color} size={size} />
                ),
                tabBarColor: "#6518F4"
            }}
        />
        <AppTabs.Screen 
            name="Appointments" 
            component={BookingsStackNavigator}
            options={{
                tabBarLabel: 'Appointments',
                tabBarIcon: ({ color, size = 24 }) => (
                    <Icon name="ios-calendar" color={color} size={size} />
                ),
                tabBarColor: "#6518F4"
            }}
        />
        <AppTabs.Screen 
            name="My Profile" 
            component={ProfileStackNavigator}
            options={{
                tabBarLabel: 'My Profile',
                tabBarIcon: ({ color, size = 24 }) => (
                    <Icon name="md-person" color={color} size={size} />
                ),
                tabBarColor: "#D02760"
            }}
        />
    </AppTabs.Navigator>
}