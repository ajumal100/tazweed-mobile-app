import React from 'react';
import { View, StyleSheet, Text, ScrollView, TouchableNativeFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { useSelector, useDispatch } from 'react-redux';

import * as authActions from '../store/actions/auth'

const ProfileScreen = () => {
    const fullName = useSelector(state => state.auth.name);
    const dispatch = useDispatch();

    const handleLogout = async () => {
        await dispatch(authActions.logout());
    }

    return <ScrollView style={styles.container}>
        <View style={styles.logo}>
            <Icon name="user-circle-o" color="#ccc" style={styles.profilePic}/>
        </View>
        <View style={styles.welcomeTextOuter}>
            <Text style={styles.welcomeText}>Welcome, {fullName}</Text>
        </View>
        <View style={styles.buttonContainer}>
            <TouchableNativeFeedback style={styles.loginOuter} onPress={handleLogout}>
                <View style={styles.loginBtnContainer}>
                    <Text style={styles.loginBtn}>LOG OUT</Text>
                </View>
            </TouchableNativeFeedback>
        </View>
    </ScrollView>;
}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        backgroundColor: "#ffffff"
    },
    logo: {
        margin: 50,
        marginBottom: "10%",
        alignItems: "center"
    },
    buttonContainer: {
        paddingHorizontal: 100,
        flexDirection: "column"
    },
    loginBtn: {
        color: "#fff",
        elevation: 0,
        fontFamily: "Opensans-Bold",
        paddingVertical: 15,
        fontSize: 16
    },
    loginOuter: {
        marginTop: 15
    },
    loginBtnContainer: {
        backgroundColor: "#dc3545",
        borderRadius: 10,
        alignItems: "center"
    },
    profilePic: {
        fontSize: 200
    },
    welcomeTextOuter: {
        marginBottom: "20%",
        alignItems: "center"
    },
    welcomeText: {
        fontFamily: "Opensans-Bold",
        fontSize: 25
    }
});

export default ProfileScreen;