import React, { useState, useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import SellersList from '../components/SellersList';
import * as sellerActions from '../store/actions/sellers';
import * as authActions from '../store/actions/auth';

const NearScreen = (props) => {
    const [isLoading, setIsLoading] = useState(false);

    const dispatch = useDispatch();

    const loadSellers = useCallback(async () => {
        try {
            await dispatch(sellerActions.getAllSellers());
        } catch (err) {
            dispatch(authActions.handleErrors(err.message));
        }
    },[]);
    
    useEffect(() => {
        let willFocusSub = props.navigation.addListener('focus', loadSellers);
        return willFocusSub;
    }, [loadSellers]);

    useEffect(() => {
        loadSellers();
    }, []);

    const sellers = useSelector(state => state.seller.allSellers);
    const handleSelect = (id, screenName) => {
        props.navigation.navigate('Seller', {id, screenName});
    }

    if(sellers.length == 0){
        return <View style={styles.emptyContainer}>
            <Icon name="emoticon-sad-outline" color="#ccc" size={80} />
            <Text style={styles.defaultText}>No Sellers Nearby</Text>
        </View>
    }
    
    return <View style={styles.container}>
                {
                    sellers && sellers.length != 0 &&
                    <SellersList sellers={sellers} handleSelect={handleSelect} customTitle="Near you"/>
                }
            </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    emptyContainer: {
        height: "100%",
        backgroundColor: "#ffffff",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 20
    },
    defaultText: {
        fontFamily: "Opensans-Regular",
        fontSize: 22,
        marginTop: 15,
        color: "#ccc",
        textAlign: "center"
    }
});

export default NearScreen;