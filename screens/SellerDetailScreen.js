import React, { useState, useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { View, StyleSheet, Text, Image, TouchableNativeFeedback, ScrollView, TouchableNativeFeedbackComponent, Alert, ActivityIndicator } from 'react-native';
import { Calendar } from 'react-native-calendars';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';

import Colors from '../utils/Colors';
import ScrollButtons from '../components/ScrollButtons';
import Pills from '../components/Pills';
import * as bookingActions from '../store/actions/bookings';
import * as authActions from '../store/actions/auth';
import API from '../utils/Api';

const getDateString = (toDate) => {
    return toDate.getFullYear() + '-' + (toDate.getMonth()+1).toString().padStart(2,"0") + '-' + toDate.getDate().toString().padStart(2,"0");
}

const SellerDetailScreen = (props) => {
    const TIME_GAP = 30;
    const TIME_FORMAT = "hh:mm A";
    const seller = useSelector(state => state.seller.allSellers.filter(business => business._id == props.route.params.id)[0]);
    const [toggle, setToggle] = useState(1);
    const [time, setTime] = useState("11:00 AM");
    const [isLoading, setIsloading] = useState(false);
    const dispatch = useDispatch();
    const scrollView = useRef(null);
    
    const now = new Date();
    const lastDate = new Date();
    lastDate.setMonth(now.getMonth()+2);
    const minDate = getDateString(now);
    const maxDate = getDateString(lastDate);
    const [date, setDate] = useState(minDate);
    const [availableTimeSlots, setAvailableTimeSlots] = useState([]);
    
    const getAllHours = (timeGap) => {
        const timeArray = [];
        const time = new Date();
        time.setHours(0,0,0,0);
        do {
            let hours = time.getHours();
            let minutes = time.getMinutes();
            hours = hours % 12;
            hours = hours ? hours : 12;
            hours = hours.toString().padStart(2,0);
            const ampm = time.getHours() >= 12 ? 'PM' : 'AM';
            minutes = minutes.toString().padStart(2,0);
            timeArray.push(hours + ':' + minutes + ' ' + ampm);
            time.setMinutes(time.getMinutes() + timeGap);
        } while(time.getHours() != time.getMinutes());
        return timeArray;
    }

    const handleTimeSelect = (tappedTime) => {
        setTime(tappedTime);
    }

    const handleDateSelect = (day) => {
        setDate(day.dateString);
        scrollView.current.scrollToEnd({animated: true});
        const date = new Date(day.dateString);
        const timeSlots = availableDays[date.getDay()];
        let newAvailableTimeSlots = {};
        for(const timeSlot of timeSlots){
            const from = moment(timeSlot['from'], TIME_FORMAT);
            const to = moment(timeSlot['to'], TIME_FORMAT);
            for(const time of allTimeSlots){
                const curTime = moment(time, TIME_FORMAT);
                if(curTime >= from && curTime <= to){
                    newAvailableTimeSlots[time] = " ";
                }
            }
        }
        newAvailableTimeSlots = Object.keys(newAvailableTimeSlots);
        newAvailableTimeSlots.sort((a,b) => {
            return moment(a, TIME_FORMAT) - moment(b, TIME_FORMAT);
        })
        setAvailableTimeSlots(newAvailableTimeSlots);
        setTime(newAvailableTimeSlots[0]);
    }

    const handleBooking = async () => {
        setIsloading(true);
        try {
            await dispatch(bookingActions.addBooking(seller._id, date, time));
            setIsloading(false);
            Alert.alert("Booking Added", "Your booking is successful. Please check Appointments for changes in Booking status.",
            [
                {text: "CHECK APPOINTMENTS", onPress: () => { props.navigation.navigate('Appointments'); }},
                {text: "OK", onPress: () => { props.navigation.pop() }}
            ]);
        } catch (err) {
            setIsloading(false);
            dispatch(authActions.handleErrors(err.message));
        }
    }

    const markedDates = {
        [date]: {selected: true, selectedColor: Colors.primary}
    }

    const allTimeSlots = getAllHours(TIME_GAP);
    const availableDays = JSON.parse(seller.slots);

    //Mark Off-days
    const offDays = [];
    for(let i = 0; i < 7; i++){
        if(!availableDays[i]){
            offDays.push(i);
        }
    }
    
    let currentDate = new Date();
    for(let iterator = 0; iterator < 90; iterator++){
        if(offDays.includes(currentDate.getDay())){
            markedDates[getDateString(currentDate)] = { disabled: true };
        }
        currentDate.setDate(currentDate.getDate()+1);
    }

    //Init Available Time slots
    useEffect(() => {
        setAvailableTimeSlots(allTimeSlots.slice());
    }, []);

    return <View style={styles.parentContainer}>
            {
                seller && <>
                <ScrollView ref={scrollView}>
                    <View style={styles.container}>
                        {
                            seller.img != "" &&
                            <View>
                                <Image resizeMode="cover" style={styles.coverImg} source={{uri: API.baseUrl + '/' + seller.img.replace(/\\/g, '/')}}/>
                            </View>
                        }
                        <View style={styles.detailsContainer}>
                            <View style={styles.servicesContainer}>
                                <Text style={styles.headerText}>Services</Text>
                                <View style={styles.borderBottom}>
                                    <Pills items={seller.services}/>
                                </View>
                            </View>
                            <View style={styles.titleContainer}>
                                <Text style={styles.headerText}>Book an Appointment</Text>
                                <View style={styles.toggleContainer}>
                                    <View style={{...styles.toggleBtn, ...styles.leftToggle, backgroundColor: toggle == 0 ? Colors.primary: "#fff"}}>
                                        <TouchableNativeFeedback onPress={()=>{ setToggle(1) }}>
                                            <View style={styles.toggleBtnInner}>
                                                <Icon name="table-row" color={toggle != 0 ? Colors.primary: "#fff"} size={25}/>
                                            </View>
                                        </TouchableNativeFeedback>
                                    </View>
                                    <View style={{...styles.toggleBtn,  ...styles.rightToggle, backgroundColor: toggle != 0 ? Colors.primary: "#fff"}}>
                                        <TouchableNativeFeedback onPress={()=>{ setToggle(1) }}>
                                            <View style={styles.toggleBtnInner}>
                                                <Icon name="calendar-outline" color={toggle == 0 ? Colors.primary: "#fff"} size={25}/>
                                            </View>
                                        </TouchableNativeFeedback>
                                    </View>
                                </View>
                            </View>
                            {
                                toggle == 1 ?
                                <View style={styles.borderBottom}>
                                    <Calendar 
                                        minDate={minDate} 
                                        maxDate={maxDate}
                                        onDayPress={handleDateSelect}
                                        markedDates={markedDates}
                                        />
                                </View> : null
                            }
                            <View style={styles.borderBottom}>
                                <ScrollButtons onSelectTime={handleTimeSelect} selectedItem={time} items={availableTimeSlots}/>
                            </View>
                        </View>
                    </View>
            </ScrollView>
            <View style={styles.bookBtnContainer}>
                <View style={styles.bookBtnInner}>
                    <TouchableNativeFeedback onPress={handleBooking}>
                        {
                            isLoading ?
                            <ActivityIndicator size="large" color={Colors.primary} /> :
                            <View style={{...styles.bookBtn, backgroundColor: isLoading ? Colors.primaryFade : Colors.primary}}>
                                <Text style={styles.bookBtnText}>BOOK</Text>
                            </View>
                        }
                    </TouchableNativeFeedback>
                </View>
            </View>
            </>
            }
        </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    parentContainer: {
        backgroundColor: "#fff",
        flex: 1,
        paddingBottom: 90
    },
    borderBottom: {
        borderBottomColor: "#ddd",
        borderBottomWidth: 1,
        paddingBottom: 10,
        marginBottom: 10
    },
    coverImg: {
        height: 200,
        width: "100%"
    },
    headerText: {
        fontFamily: "Opensans-Bold",
        fontSize: 14
    },
    titleContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 15
    },
    servicesContainer: {
        justifyContent: "space-between",
        padding: 15
    },
    toggleContainer: {
        flexDirection: "row"
    },
    toggleBtn: {
        borderWidth: 2,
        borderColor: Colors.primary,
        borderRadius: 8,
    },
    toggleBtnInner: {
        padding: 5,
        paddingHorizontal: 10
    },
    leftToggle: {
        borderTopRightRadius: 0,
        borderBottomRightRadius: 0
    },
    rightToggle: {
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0
    },
    bookBtnContainer: {
        backgroundColor: "#fff",
        position: "absolute",
        bottom: 0,
        width: "100%",
        padding: 20
    },
    bookBtn: {
        padding: 12
    },
    bookBtnInner: {
        borderRadius: 8,
        overflow: "hidden"
    },
    bookBtnText: {
        textAlign: "center",
        color: "#fff",
        fontFamily: "Opensans-Bold",
        fontSize: 15
    }
});

export default SellerDetailScreen;