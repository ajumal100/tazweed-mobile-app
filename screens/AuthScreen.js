import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { View, StyleSheet, Alert, ScrollView, TextInput, Button, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import * as authActions from '../store/actions/auth';
import Colors from '../utils/Colors';
import Errors from '../utils/Errors';

const AuthScreen = (props) => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [focussed, setFocus] = useState(0);
    const [isLoading, setIsloading] = useState(false);

    const dispatch = useDispatch();

    const handleSubmit = async () => {
        const type = props.route.params.type;
        
        if(email == "" || !(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/).test(email)){
            Alert.alert("Email is incorrect", "Please enter a valid email ID");
            return;
        }
        if(password == ""){
            Alert.alert("Password", "Please enter a password");
            return;
        } 

        try{
            if(type == 'login'){
                setIsloading(true);
                await dispatch(authActions.login(email, password));
            } else {
                if(password.length < 6){
                    Alert.alert("Password", "Password must be atleast 6 characters long");
                    return;
                }
                if(name == ""){
                    Alert.alert("Name not entered", "Please enter your Full name");
                    return;
                }
                setIsloading(true);
                await dispatch(authActions.signup(name, email, password));
            }
        } catch(err) {
            if(err.message == Errors.USER_NOT_FOUND){
                Alert.alert("Can't Find Account", 
                    `It looks like ${email} doesn't match an existing account. If you don't have an account, you can create one now.`,
                    [
                        {text: "CREATE ACCOUNT", onPress: () => { props.navigation.setParams({type:'signup'}); }},
                        {text: "OK", type: 'Cancel'}
                    ]);
            } else if(err.message == Errors.AUTH_FAIL){
                Alert.alert("Incorrect Password", 
                    `The password you entered is incorrect. Please try again.`,
                    [
                        {text: "OK", type: 'Cancel'}
                    ]);
            } else if(err.message == Errors.USER_EXISTS) {
                Alert.alert("Email Account Exists", 
                    `There is an existing account associated with this email`,
                    [
                        {text: "OK", type: 'Cancel'}
                    ]);
            } else {
                dispatch(authActions.handleErrors(err.message));
            }
            setIsloading(false);
        }
    }

    return <View>
        <ScrollView style={styles.container} keyboardShouldPersistTaps={'handled'}>
            <View style={styles.form}>
                {
                    props.route.params.type != "login" && 
                    <View style={{...styles.formControl, borderBottomColor: focussed == 1 ? "#FB7F5B":"#ccc", borderBottomWidth: focussed == 1 ? 2:1}}>
                        <Icon name="account-outline" color={focussed == 1 ? "#FB7F5B":"#ccc"} style={{fontSize:25, paddingTop:5}}/>
                        <TextInput
                            style={styles.input}
                            value={name}
                            placeholder="Your Full Name"
                            autoCompleteType="name"
                            onChangeText={text => setName(text)}
                            onFocus={()=>{setFocus(1)}}
                            onBlur={()=>{setFocus(0)}}
                        />
                    </View>
                }
                <View style={{...styles.formControl, borderBottomColor: focussed == 2 ? "#FB7F5B":"#ccc", borderBottomWidth: focussed == 2 ? 2:1}}>
                    <Icon name="email-outline" color={focussed == 2 ? "#FB7F5B":"#ccc"} style={{fontSize:23, paddingTop:7}}/>
                    <TextInput
                        style={styles.input}
                        value={email}
                        placeholder="Your Email"
                        autoCompleteType="email"
                        keyboardType="email-address"
                        onChangeText={text => setEmail(text)}
                        onFocus={()=>{setFocus(2)}}
                        onBlur={()=>{setFocus(0)}}
                    />
                </View>
                <View style={{...styles.formControl, borderBottomColor: focussed == 3 ? "#FB7F5B":"#ccc", borderBottomWidth: focussed == 3 ? 2:1}}>
                    <Icon name="lock-outline" color={focussed == 3 ? "#FB7F5B":"#ccc"} style={{fontSize:23, paddingTop:5}}/>
                    <TextInput
                        style={styles.input}
                        value={password}
                        placeholder={props.route.params.type == "login" ? "Password":"Create Password"}
                        secureTextEntry={true}
                        onChangeText={text => setPassword(text)}
                        onFocus={()=>{setFocus(3)}}
                        onBlur={()=>{setFocus(0)}}
                    />
                </View>
            </View>
            <View style={styles.btnContainer}>
                {isLoading ? (
                    <ActivityIndicator size="small" color={Colors.start} />
                ) : (
                    <Button style={styles.createUserBtn} color="#FB7F5B" title={props.route.params.type == "login" ? "SIGN IN":"CREATE ACCOUNT"} onPress={handleSubmit}/>
                )}
            </View>
        </ScrollView>
    </View>
}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        backgroundColor: "#ffffff"
    },
    form: {
        margin: 20
    },
    formControl: {
        width: '100%',
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        flex: 1,
        flexDirection: "row",
        marginBottom: 20,
        height: 40
    },
    label: {
        fontFamily: 'open-sans-bold',
        marginVertical: 8
    },
    input: {
        marginLeft: 7,
        paddingVertical: 5,
        width: "100%"
    },
    btnContainer: {
        marginHorizontal: "30%",
        paddingVertical: 20
    }
});

export default AuthScreen;