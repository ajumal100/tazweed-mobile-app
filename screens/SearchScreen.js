import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { View, StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

import SellersList from '../components/SellersList'

const SearchScreen = (props) => {
    const sellers = useSelector(state => state.seller.sellers);

    const handleSelect = (id, screenName) => {
        props.navigation.navigate('Seller', {id, screenName});
    }
    
    return <View style={styles.container}>
        {
            !sellers || sellers.length == 0 ?
            <View style={styles.container}>
                <Icon name="compass-outline" color="#ccc" size={80} />
                <Text style={styles.defaultText}>Search for Service Providers near you</Text>
            </View> :
            <SellersList sellers={sellers} handleSelect={handleSelect} customTitle="Results"/>
        }
    </View>
}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        backgroundColor: "#ffffff",
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    defaultText: {
        fontFamily: "Opensans-Regular",
        fontSize: 22,
        marginTop: 15,
        color: "#ccc"
    }
});

export default SearchScreen;