import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, ScrollView, Image, Button, TouchableOpacity, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { useDispatch } from 'react-redux';

import * as authActions from '../store/actions/auth';
import Colors from '../utils/Colors'

const StartScreen = (props) => {
    const [savedData, setSavedData] = useState('');
    const [autoLoginCheck, setAutoLoginCheck] = useState(true);
    const dispatch = useDispatch();
    const handleLogin = (type) => {
        props.navigation.navigate("Auth", {type});
    }

    AsyncStorage.getItem('@auth', (err, result) => {
        if(!result){
            setAutoLoginCheck(false);
        }
        if(!err && result) {
            setSavedData(result);
        }
    });

    useEffect(() => { 
        if(savedData){
            const data = JSON.parse(savedData.trim());
            if(data.token){
                dispatch(authActions.autoLogin(data));
            } else {
                setAutoLoginCheck(false);
            }
        }
    },[savedData]);

    if(autoLoginCheck) {
        return (
            <View style={styles.screen}>
                <ActivityIndicator size="large" color={Colors.primary} />
                <Text>Logging you in...</Text>
            </View>
        );
    }

    return <View style={styles.container}>
        <ScrollView contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.innerContainer}>
                <View style={styles.logo}>
                    <Image source={require('../assets/img/logo.png')}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button style={styles.createUserBtn} color="#FB7F5B" title="CREATE ACCOUNT" onPress={handleLogin.bind(this,"create")}/>
                    <TouchableOpacity style={styles.loginOuter} onPress={handleLogin.bind(this,"login")}>
                        <View style={styles.loginBtnContainer}>
                            <Text style={styles.loginBtn}>SIGN IN</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    innerContainer: {
        flexDirection: "column",
        backgroundColor: "#fff",
        height: "100%"
    },
    logo: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1
    },
    buttonContainer: {
        paddingHorizontal: 100,
        flexDirection: "column",
        justifyContent: "center",
        backgroundColor: "#fff",
        flex: 1
    },
    createUserBtn: {
        fontFamily: "Opensans-Bold"
    },
    loginBtn: {
        color: "#FB7F5B",
        elevation: 0,
        fontFamily: "Opensans-Bold",
        paddingVertical: 20
    },
    loginBtnContainer: {
        flex: 1,
        alignItems: "center"
    },
    loginOuter: {
        marginTop: 15
    },
    screen: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default StartScreen;