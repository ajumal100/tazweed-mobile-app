import React, { useState, useEffect, useCallback } from 'react';
import { View, StyleSheet, Text, FlatList, TouchableOpacity, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import * as bookingActions from '../store/actions/bookings';
import * as authActions from '../store/actions/auth';
import Colors from '../utils/Colors';

const BookingsScreen = (props) => {
    const [isRefreshing, setIsRefreshing] = useState(false);
    const bookings = useSelector(state => state.booking.bookings);
    
    const dispatch = useDispatch();

    const loadBookings = useCallback(async () => {
        try {
            await dispatch(bookingActions.getBookings());
        } catch (err) {
            dispatch(authActions.handleErrors(err.message));
        }
    },[]);
    
    useEffect(() => {
        let willFocusSub = props.navigation.addListener('focus', loadBookings);
        return willFocusSub;
    }, [loadBookings]);

    useEffect(() => {
        loadBookings();
    }, []);

    const handleDeleteBooking = async (id) => {
        try {
            await dispatch(bookingActions.deleteBooking(id));
        } catch (err) {
            dispatch(authActions.handleErrors(err.message));
        }
    }

    const handleDeleteBtn = (id) => {
        Alert.alert("Cancel Booking", 
            `You are about to cancel the booking. Do you want to continue?`,
            [
                {text: "CANCEL BOOKING", onPress: () => { handleDeleteBooking(id); }},
                {text: "NO", type: 'Cancel'}
            ]);
    }

    const handleRefresh = async () => {
        setIsRefreshing(true);
        await loadBookings();
        setIsRefreshing(false);
    }

    if(bookings.length == 0){
        return <View style={styles.emptyContainer}>
            <Icon name="calendar-times-o" color="#ccc" size={80} />
            <Text style={styles.defaultText}>You have not created any appointments yet.</Text>
        </View>
    }

    return <View style={styles.container}>
        <FlatList 
            refreshing={isRefreshing}
            onRefresh={handleRefresh}
            data={bookings} 
            keyExtractor={booking => booking._id}
            contentContainerStyle={styles.flatlistStyle}
            renderItem={({item}) => {
                const booking = item;
                const date = new Date(booking.date);
                const month = date.toDateString().split(' ')[1].toUpperCase();
                return <View key={booking._id} style={styles.bookingContainer}>
                <View style={styles.bookingStatusContainer}>
                    <View style={{...styles.status, ...styles[booking.status]}}>
                        <Text style={styles.statusText}>{booking.status.toUpperCase()}</Text>
                    </View>
                    <View>
                        <Text style={styles.sellerText}>{booking.seller.name}</Text>
                        <Text style={styles.sellerAreaText}>{booking.seller.location}</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => { handleDeleteBtn(booking._id) }}>
                            <View style={styles.actionBtn}>
                                <Text style={styles.actionBtnText}>CANCEL</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.timeContainer}>
                    <Text style={styles.textArea}>{month}</Text>
                    <Text style={styles.date}>{date.getDate()}</Text>
                    <Text style={styles.textArea}>{booking.time}</Text>
                </View>
            </View>
        }}/>
    </View>
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    emptyContainer: {
        height: "100%",
        backgroundColor: "#ffffff",
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        padding: 20
    },
    defaultText: {
        fontFamily: "Opensans-Regular",
        fontSize: 22,
        marginTop: 15,
        color: "#ccc",
        textAlign: "center"
    },
    flatlistStyle: {
        padding: 15,
        paddingTop: 20
    },
    bookingContainer: {
        flexDirection: "row",
        width: "100%",
        borderWidth: 2,
        borderColor: "#ccc",
        borderRadius: 15,
        marginBottom: 15
    },
    bookingStatusContainer: {
        flex: 3,
        borderRightWidth: 2,
        borderColor: "#ccc",
        padding: 10
    },
    timeContainer: {
        flex: 1,
        padding: 10,
        alignItems: "center",
        justifyContent: "center"
    },
    date: {
        fontFamily: "Opensans-Regular",
        fontSize: 26
    },
    textArea: {
        fontFamily: "Opensans-Regular",
        fontSize: 13
    },
    status: {
        alignSelf: "flex-start",
        paddingVertical: 3,
        paddingHorizontal: 6,
        borderRadius: 10,
        marginBottom: 15
    },
    approved:{
        backgroundColor: "#5CB85C"
    },
    rejected:{
        backgroundColor: "#D9534F"
    },
    pending: {
        backgroundColor: "#F0AD4E"
    },
    statusText: {
        fontSize: 11,
        color: "#fff"
    },
    sellerText: {
        fontFamily: "Opensans-Bold"
    },
    sellerAreaText: {
        fontFamily: "Opensans-Regular",
        color: "#666"
    },
    actionBtn: {
        marginTop: 10,
        paddingVertical: 5,
        paddingHorizontal: 15,
        backgroundColor: Colors.primary,
        borderRadius: 7
    },
    actionBtnText: {
        textAlign: "center",
        color: "#fff"
    }
});

export default BookingsScreen;