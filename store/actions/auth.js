import API from '../../utils/Api';
import Errors from '../../utils/Errors';
import { Alert } from 'react-native';

export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const SIGNUP = "SIGNUP";
export const AUTO_LOGIN = "AUTO_LOGIN";

export const login = (email, password) => {
    return async dispatch => {
        const response = await fetch(API.baseUrl + '/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email,
                password
            })
        });

        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: LOGIN, data: responseData});
    }
}

export const logout = () => {
    return {type: LOGOUT};
}

export const signup = (name, email, password) => {
    return async dispatch => {
        const response = await fetch(API.baseUrl + '/users/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name,
                email,
                password
            })
        });

        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: SIGNUP, data: responseData});
    }
}

export const autoLogin = (data) => {
    return async (dispatch, getState) => {
        dispatch({type: AUTO_LOGIN, data});
    }
}

export const handleErrors = (err) => {
    return async dispatch => {
        if(err == Errors.JWT_EXPIRED || err == Errors.JWT_MALFORMED || Errors.NO_JWT){
            Alert.alert('Session Expired', 'Please log in again');
            dispatch({type: LOGOUT});
        } else {
            console.log("UNHANDLED Error: ", err);
        }
    }
}