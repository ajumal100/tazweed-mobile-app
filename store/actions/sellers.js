import API from '../../utils/Api'

export const GET_ALL_SELLERS = "GET_ALL_SELLERS";
export const GET_SELLERS = "GET_SELLERS";

export const getAllSellers = () => {
    return async (dispatch, getState) => {
        //GET /sellers/
        const response = await fetch(API.baseUrl + '/sellers/',{
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + getState().auth.token
            }
        });
        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: GET_ALL_SELLERS, data: responseData});
    }
}

export const getSellers = (query) => {
    return async (dispatch, getState) => {
        //GET /sellers/search/:query
        const response = await fetch(API.baseUrl + '/sellers/search/' + query,{
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + getState().auth.token
            }
        });
        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: GET_SELLERS, data: responseData});
    }
}