import API from '../../utils/Api';

export const ADD_BOOKING = "ADD_BOOKING";
export const GET_BOOKING = "GET_BOOKING";
export const DELETE_BOOKING = "DELETE_BOOKING";

export const addBooking = (seller, date, time) => {
    return async (dispatch, getState) => {
        //POST /bookings/
        const response = await fetch(API.baseUrl + '/bookings/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + getState().auth.token
            },
            body: JSON.stringify({
                user: getState().auth.id,
                seller,
                date,
                time
            })
        })

        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: ADD_BOOKING, data: responseData});
    }
}

export const getBookings = () => {
    return async (dispatch, getState) => {
        //GET /bookings/user/:id
        const response = await fetch(API.baseUrl + '/bookings/user/' + getState().auth.id, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + getState().auth.token
            }
        })

        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: GET_BOOKING, data: responseData});
    }
}

export const deleteBooking = (id) => {
    return async (dispatch, getState) => {
        //DELETE /bookings/:id
        const response = await fetch(API.baseUrl + '/bookings/' + id, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + getState().auth.token
            }
        })

        const responseData = await response.json();
        if(!response.ok){
            throw new Error(responseData.errors.message);
        }
        dispatch({type: DELETE_BOOKING, data: responseData});
    }
}