import { ADD_BOOKING, GET_BOOKING, DELETE_BOOKING } from '../actions/bookings';
import Booking from '../../models/booking';

const initialState = {
    bookings: []
}

export default (state = initialState, action) => {
    switch(action.type){
        case ADD_BOOKING:
            const newBooking = new Booking(
                action.data._id, 
                action.data.user, 
                action.data.seller, 
                action.data.date, 
                action.data.time, 
                action.data.status);
            return {
                bookings: state.bookings.concat(newBooking)
            };
        case GET_BOOKING:
            return {
                bookings: action.data
            }
        case DELETE_BOOKING:
            const updatedBookings = state.bookings.filter(booking => booking._id != action.data._id);
            return {
                bookings: updatedBookings
            }
        default:
            return state;
    }
}