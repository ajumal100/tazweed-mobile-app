import { GET_ALL_SELLERS, GET_SELLERS } from '../actions/sellers';

const initialState = {
    allSellers: [],
    sellers: []
}

export default (state = initialState, action) => {
    switch(action.type){
        case GET_ALL_SELLERS:
            return {
                ...state,
                allSellers: action.data
            };
        case GET_SELLERS:
            return {
                ...state,
                sellers: action.data
            };
        default:
            return state;
    }
}