import { LOGIN, LOGOUT, SIGNUP, AUTO_LOGIN } from '../actions/auth'
import AsyncStorage from '@react-native-community/async-storage';

const initialState = {
    isAuthenticated: false,
    id: '',
    name: '',
    email: '',
    phone: '',
    token: ''
}

export default (state = initialState, action) => {
    switch(action.type){
        case LOGIN:
            if(action.data.token){
                const saveObj = {
                    token: action.data.token,
                    id: action.data.user._id,
                    name: action.data.user.name,
                    email: action.data.user.email,
                    phone: action.data.user.phone
                }
                AsyncStorage.setItem('@auth', JSON.stringify(saveObj));
            } else {
                return;
            }
            return {
                ...state,
                token: action.data.token,
                id: action.data.user._id,
                name: action.data.user.name,
                email: action.data.user.email,
                phone: action.data.user.phone,
                isAuthenticated: true
            };
        case SIGNUP:
            if(action.data.token){
                const saveObj = {
                    token: action.data.token,
                    id: action.data.user._id,
                    name: action.data.user.name,
                    email: action.data.user.email,
                    phone: action.data.user.phone
                }
                AsyncStorage.setItem('@auth', JSON.stringify(saveObj));
            } else {
                return;
            }
            return {
                ...state,
                token: action.data.token,
                id: action.data.user._id,
                name: action.data.user.name,
                email: action.data.user.email,
                phone: action.data.user.phone,
                isAuthenticated: true
            };
        case LOGOUT:
            AsyncStorage.removeItem('@auth');
            return initialState;
        case AUTO_LOGIN:
            return {
                ...state,
                ...action.data,
                isAuthenticated: true
            }
        default:
            return state;
    }
}