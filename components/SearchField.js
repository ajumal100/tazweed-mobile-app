import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { View, TextInput, StyleSheet, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import Colors from '../utils/Colors';
import * as authActions from '../store/actions/auth';
import * as sellerActions from '../store/actions/sellers';

let timer = null;

const SearchField = () => {
    const [query, setQuery] = useState('');
    const [focussed, setFocus] = useState(false);
    const [isLoading, setIsloading] = useState(false);

    const dispatch = useDispatch();
    
    const handleQuerySubmit = async (text) => {
        setIsloading(true);
        try {
            if(text.length > 2)
                await dispatch(sellerActions.getSellers(text));
            setIsloading(false);
        } catch (err) {
            setIsloading(false);
            dispatch(authActions.handleErrors(err.message));
        }
    }

    const handleQueryChange = (text) => {
        setQuery(text);
        setIsloading(false);
        if(text.length > 2){
            if(timer){
                clearTimeout(timer);
            }
            timer = setTimeout(() => {
                handleQuerySubmit(text)
            }, 800);
        }
    }

    return <View style={styles.headerContainer}>
        <View style={{...styles.formControl, borderBottomWidth: focussed ? 2:1}}>
            <Icon name="md-search" color={focussed ? "#666":"#ccc"} style={{...styles.icon, fontSize:23, paddingTop:5}}/>
            <TextInput
                style={styles.input}
                value={query}
                placeholder="What are you looking for?"
                onChangeText={handleQueryChange}
                //onFocus={()=>{setFocus(true)}}
                //onBlur={()=>{setFocus(false)}}
                returnKeyType="search"
                onSubmitEditing={handleQuerySubmit}
            />
            {isLoading && <ActivityIndicator  size="small" color={Colors.header} />}
        </View>
    </View>
}

const styles = StyleSheet.create({
    headerContainer: {
        height: 56,
        backgroundColor: Colors.header,
        padding: 10
    },
    formControl: {
        borderBottomColor: '#fff',
        borderBottomWidth: 0,
        flex: 1,
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: "#fff",
        borderRadius: 5,
        paddingHorizontal: 15
    },
    input: {
        marginLeft: 10,
        fontFamily: "Opensans-Regular",
        backgroundColor: "#ccc",
        alignItems: "center",
        flex: 1,
        textAlignVertical: "center",
        height: 40,
        backgroundColor: "transparent",
        position: "relative",
        top: 3
    },
    icon: {
        marginTop: 0,
        width: 20
    }
});

export default SearchField;