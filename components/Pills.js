import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Colors from '../utils/Colors';

const Pills = ({items}) => {
    return <View style={styles.container}>
            {
                items.map((item, i) => {
                    return <View key={i} style={styles.pill}>
                            <Text style={styles.pillText}>
                                {item}
                            </Text>
                        </View>
                })
            }
        </View>
}

const styles = StyleSheet.create({
    pill: {
        alignSelf: "flex-start",
        marginRight: 10,
        marginBottom: 5
    },
    pillText: {
        color: Colors.primary,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: Colors.primary,
        backgroundColor: "#fff",
        paddingVertical: 5,
        paddingHorizontal: 15,
        marginBottom: 5,
        fontFamily: "Opensans-Regular"
    },
    container: {
        paddingTop: 15,
        paddingBottom: 5,
        flexDirection: "row",
        flexWrap: "wrap",
        flex: 1,
    }
});

export default Pills;