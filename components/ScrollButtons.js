import React from 'react';
import { View, ScrollView, Text, StyleSheet, TouchableOpacity } from 'react-native';

import Colors from '../utils/Colors';

const ScrollButtons = (props) => {
    return <View style={styles.scrollContainer}>
        <ScrollView showsHorizontalScrollIndicator={false} horizontal={true}>
            {
                props.items.map((item, index) => {
                    return <TouchableOpacity key={index} activeOpacity={0.5} onPress={props.onSelectTime.bind(this, item)}>
                                <View style={{
                                    ...styles.pill, 
                                    marginLeft: index == 0 ? 15:0, 
                                    borderColor: item == props.selectedItem ? Colors.primary: "#ccc",
                                    backgroundColor: item == props.selectedItem ? Colors.primaryFade: "#fff"}}>
                                    <Text style={{...styles.pillText, fontFamily: item == props.selectedItem ? "Opensans-Bold": "Opensans-Regular"}}>{item}</Text>
                                </View>
                            </TouchableOpacity>
                })
            }
        </ScrollView>
    </View>
}

const styles = StyleSheet.create({
    scrollContainer: {
        paddingVertical: 5
    },
    pill: {
        borderRadius: 6,
        borderWidth: 1,
        paddingHorizontal: 20,
        paddingVertical: 8,
        marginRight: 13,
        overflow: "hidden"
    },
    pillText: {
        fontFamily: "Opensans-Regular"
    }
});

export default ScrollButtons;