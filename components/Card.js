import React from 'react';
import { View, Text, StyleSheet, Image, TouchableNativeFeedback, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Colors from '../utils/Colors';
import API from '../utils/Api';

const Card = ({item, onSelect}) => {
    return  <View style={styles.cardOuter}>
                <TouchableNativeFeedback activeOpacity={0.8} useForeground={true} onPress={onSelect.bind(this,item._id, item.name)}>
                    <View>
                        {
                            item.img != "" && 
                            <View style={styles.cover}>
                                <Image resizeMode="cover" style={styles.coverImg} source={{uri: API.baseUrl + '/' + item.img.replace(/\\/g, '/')}}/>
                                <Text>{item.img}</Text>
                            </View>
                        }
                        <View style={styles.textContainer}>
                            <Text style={styles.nameText}>{item.name}</Text>
                            <View style={styles.ratingContainer}>
                                <Icon name="star" color="#fff" size={18}/>
                                <Text style={styles.ratingText}>{item.rating}</Text>
                            </View>
                        </View>
                        {
                            item.type != "" &&
                            <View>
                                <Text style={styles.typeText}>{item.type.charAt(0).toUpperCase() + item.type.substr(1)}</Text>
                            </View>
                        }
                        {
                            item.location != "" &&
                            <View>
                                <Text style={styles.locationText}>{item.location}</Text>
                            </View>
                        }
                    </View>
                </TouchableNativeFeedback>
            </View>
}

const styles = StyleSheet.create({
    cardOuter: {
        marginBottom: 20,
        borderBottomColor: "#ccc",
        borderBottomWidth: 1,
        paddingBottom: 15,
        overflow: "hidden"
    },
    nameText: {
        fontFamily: "Opensans-Bold",
        fontSize: 18
    },
    cover: {
        width: "100%",
        height: 190,
        borderRadius: 7,
        overflow: "hidden",
        backgroundColor: "#000"
    },
    textContainer: {
        paddingTop: 10,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    coverImg: {
        height: "100%",
        width: "100%"
    },
    ratingContainer: {
        padding: 5,
        paddingRight: 7,
        backgroundColor: "#57A722",
        borderRadius: 5,
        flexDirection: "row"
    },
    ratingText: {
        color: "#fff",
        fontFamily: "Opensans-Bold",
        fontSize: 15,
        marginLeft: 2
    },
    typeText: {
        fontFamily: "Opensans-Bold",
        fontSize: 11,
        color: "#444"
    },
    locationText: {
        fontFamily: "Opensans-Regular",
        fontSize: 10,
        color: "#666"
    },
    bookBtn: {
        backgroundColor: Colors.primary,
        padding: 8,
        marginTop: 5,
        borderRadius: 8,
        width: "45%"
    },
    bookBtnText: {
        color: "#fff",
        fontFamily: "Opensans-Bold",
        textAlign: "center"
    },
    bookBtnOuter: {
        alignItems: "flex-end"
    }
});

export default Card;