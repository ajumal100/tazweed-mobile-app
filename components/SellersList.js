import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';

import Card from '../components/Card';

const SellersList = (props) => {
    return <View style={styles.container}>
                <View>
                    <FlatList style={styles.listContainer} keyExtractor={seller => seller._id} data={props.sellers} renderItem={({index, item}) => {
                        if(index == 0){
                            return <View>
                                    <Text style={styles.resultText}>{props.customTitle} ({props.sellers.length})</Text>
                                    <Card item={item} onSelect={props.handleSelect}/>
                                </View>
                        }
                        return <Card item={item} onSelect={props.handleSelect}/>
                    }} />
                </View>
            </View>;
}

const styles = StyleSheet.create({
    container: {
        height: "100%",
        width: "100%",
        backgroundColor: "#fff",
        flex: 1
    },
    resultText: {
        fontFamily: "Opensans-Bold",
        fontSize: 24,
        marginTop: 10,
        marginBottom: 20
    },
    listContainer: {
        paddingBottom: 100,
        paddingHorizontal: 15
    }
});

export default SellersList;