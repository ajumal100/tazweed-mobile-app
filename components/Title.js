import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Title = ({text}) => {
    return <View>
        <Text style={styles.title}>{text}</Text>
    </View>
}

const styles = StyleSheet.create({
    title: {
        fontFamily: "Opensans-Regular",
        fontSize: 24
    }
});

export default Title;